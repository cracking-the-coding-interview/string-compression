1.6: String Compression
======================================

### STATUS:
* master: [![pipeline status](https://gitlab.com/cracking-the-coding-interview/string-compression/badges/master/pipeline.svg)](https://gitlab.com/cracking-the-coding-interview/string-compression/commits/master)
* develop: [![pipeline status](https://gitlab.com/cracking-the-coding-interview/string-compression/badges/develop/pipeline.svg)](https://gitlab.com/cracking-the-coding-interview/string-compression/commits/develop)

======================================
### Table of Contents
1. [Running Tests](https://gitlab.com/cracking-the-coding-interview/string-compression#running-tests)
2. [Problem Statement](https://gitlab.com/cracking-the-coding-interview/string-compression#problem-statement)
3. [Questions](https://gitlab.com/cracking-the-coding-interview/string-compression#questions)
4. [Solution](https://gitlab.com/cracking-the-coding-interview/string-compression#solution)
5. [Implementation](https://gitlab.com/cracking-the-coding-interview/string-compression#implementation)
6. [Additional notes](https://gitlab.com/cracking-the-coding-interview/string-compression#additional-notes)
7. [References](https://gitlab.com/cracking-the-coding-interview/string-compression#running-tests)

# Running Tests:
I'm using Gulp as a task runner, eslint
(with airbnb coding  standards) for code linting,
and mocha as a testing framework.  To run tests, after a fresh
clone of this repository, run:
```
npm install
npm test
```
The grunt test task will run the lint and mocha tasks sequentially.  Those
tasks will run eslint, and mocha.

# Problem Statement:

Implement a method to perform basic string compression using the counts of
repeated characters.  For example, the string aabcccccaaa would become
a2b1c5a3.  If the 'compressed' string would not become smaller than the
original string, your method should return the original string.
You can assume that the string has only uppercase and lowercase characters
(a-z).

# Questions:
  1. I'm assuming that this method should return an empty string when
  given an empty string.  Is that correct?

  2. Should I be ignoring repeated substrings?

# Solution:

Thankfully we only need to worry about uppercase and lowercase letters a-z.

I would just iterate through the string linearly, and count the occurrence
of each letter.  I'd count the number of times a character is repeated, then
when I see a new character, write the character and the number of times it
occurred to the 'compressed string' output.

Once I had the 'compressed string' output, I'd compare the length of that to
the length of the input string.  Then I'd output the shortest string.

# Implementation:

We won't need any special handling, since these strings will only have the
characters A-Z or a-z.  No unicode characters.

If the input string is null, throw an exception.
If the input string is not a string, throw an exception.

Process the input string, and build the output string as described in the
solution.

Compare the lengths of the input string and output strings, and return the
shortest one.  If the lengths are equal, return the input string.

# Additional notes:

# References:
