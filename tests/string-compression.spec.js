const assert = require('assert');
const stringCompressionModule = require('../src/string-compression');

describe('String Compression Module Tests:', () => {
  describe('compressString():', () => {
    it('should throw exception when input string is null', () => {
      assert.throws(() => {
        stringCompressionModule.compressString(null);
      }, Error, 'input string str is null.');
    });

    it('should throw exception when input string is not a string', () => {
      assert.throws(() => {
        stringCompressionModule.compressString(42);
      }, Error, 'input string str is not a string.');
    });

    it('should return compressed string for valid input string. (compressed shorter)', () => {
      const input = 'aaBcccccaaa';
      const expected = 'a2B1c5a3';
      const actual = stringCompressionModule.compressString(input);

      assert.equal(actual, expected);
    });

    it('should return input string for valid input string. (input shorter)', () => {
      const input = 'aBcdabcdAbcd'; // a1b1c1d1a1b1c1d1a1b1c1d1
      const expected = 'aBcdabcdAbcd';
      const actual = stringCompressionModule.compressString(input);

      assert.equal(actual, expected);
    });

    it('should return input string for valid input string. (equal lengths)', () => {
      const input = 'AAbccaaa'; // a2b1c2a3
      const expected = 'AAbccaaa';
      const actual = stringCompressionModule.compressString(input);

      assert.equal(actual, expected);
    });
  });
});
